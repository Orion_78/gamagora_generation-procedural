import itertools as it

def euclidean_dist(p1, p2):
    return (p1 - p2).get_length()

class Case(object):
    def __init__(self, coord):
        self.coord = coord
        self.data  = []


    def __repr__(self):
        return "Case(%s,%s):: " %(self.coord) + repr( self.data )


# closest_point :: case, pt -> [ (pt, distance) ]
    def distancesToPoints(self, pt, dist_func=None):
        if dist_func is None:
            dist_func = euclidean_dist
        result = []
        for p in self.data:
            # dist :: pt, pt -> distance
            distance = dist_func(pt, p)
            #distance = (pt - p).get_length()
            result.append( (p, distance) )
        return result

class Grid(object):
    def __init__(self, case_size):
        self.case_size = case_size
        self.cases = {}

# add_feature_point :: grid, pt -> grid
    def add_feature_point(self, pt):
        case = self.locate(pt)
        case.data.append(pt)

# locate :: grid, pt -> case
    def locate(self, pt):
        x = pt.x / self.case_size
        y = pt.y / self.case_size
        coord = (x, y)
        return self.get_case(coord)

    def get_case(self, coord):
        case = self.cases.get(coord)
        if case is None:
            case = Case(coord)
            self.cases[coord] = case
        return case

# neighbours :: grid, case -> [ case ]
    def neighbours(self, case):
        result = []
        x, y = case.coord
        for xx, yy in it.product([-1,0,1], repeat=2):
            result.append( self.get_case((xx+x, yy+y)) )
        return result

    def __repr__(self):
        t = ""
        for c in self.cases:
            t += c.__repr__()
        return t