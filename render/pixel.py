import numpy as np 
from PIL import Image

class ColorArray(object):
    def __init__(self, w, h):
        self.data =  np.zeros( (w,h,3), dtype=np.uint8)

    def save(self, path="my.png"):
        img = Image.fromarray(self.data, 'RGB')
        img.save(path)

    def get_pixel(self, x, y):
        return self.data[ [x], [y], : ][0]

    def set_pixel(self, x, y, color):
        self.data[ [x], [y], : ] = color

    def set_column(self, y, color):
        self.data[ :, [y], : ] = color

    def set_line(self, x, color):
        self.data[ x ] = color

