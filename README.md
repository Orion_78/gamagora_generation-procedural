Projet Divers

Perlin noise 1D, Worley et Voronoi

![Voronoi.png](https://bitbucket.org/repo/gekb6G/images/2556965000-Voronoi.png)

Koch avec un framwork Turtle fabriqué

![koch.png](https://bitbucket.org/repo/gekb6G/images/662512510-koch.png)


Génération d'arbre avec une grammaire

Génération procédural d'un labyrinthe à l'aide d'un système multi-agent et de voxel en Python. 
Puis utilisation de l'algorithme de Marching Cube pour générer le mesh correspondant.
Puis séparation du mesh en plusieur mesh plus petit pour des raisons de performance.

Ce labyrinthe sera utilisé dans un jeux où le joueur doit piloter un vaisseau à l'intérieur de l’astéroïde jusqu'à sont centre.

============== Rendu visuel et Game Play sur Unity =====================

Voxels:
Asteroide en construction
![buildingWorm1.png](https://bitbucket.org/repo/gekb6G/images/2751572144-buildingWorm1.png)

Asteroide vue interne
![insideWorm1.png](https://bitbucket.org/repo/gekb6G/images/2957192612-insideWorm1.png)

Asteroide vue externe
![outsideWorm1.png](https://bitbucket.org/repo/gekb6G/images/485453914-outsideWorm1.png)


Meshs

Vue interne

![interne.png](https://bitbucket.org/repo/gekb6G/images/2033410585-interne.png)

Vue externe

![externe.png](https://bitbucket.org/repo/gekb6G/images/1983136705-externe.png)

Vue GamePlay

![gameplay.png](https://bitbucket.org/repo/gekb6G/images/471546725-gameplay.png)


============== Retours ============================================

Version en voxels

Professeur

1. Cube trop grand, donc pas organique. 

      * Augmenter le nombre de cube et réduire leur taille.

Peyre Julien, Game designer

1. Fait très circulaire

      * Avantage : pas d’endroit ni d'envers du coup on s'y perd

2. Endroit plat au milieu (bug corrigé)

3. Aucun lien entre la sphère et les tunnels

      * Exemple : modifier des paramètres de la sphère en fonction de sa taille (fais, puisque désormais le nombre de vers est proportionnel à la taille de la sphère)

Jimenez Wilka, Game Designer

1. Directionnal lighting ignoble (corrigé dans la version astéroïdes)

2. Endroit plat au milieu (bug corrigé)

3. Cube trop gros proportionnellement au vaisseau

4. Ajouter des structure de salle reconnaissable pour moins perdre le joueur

       * Par exemple des salle de couleur différent, ou avec une géométrie particulière