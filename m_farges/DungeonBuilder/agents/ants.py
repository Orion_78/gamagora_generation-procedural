# tick
import random

import vector2d as v2d



left  = v2d.Vec2d( -1, 0)
right = v2d.Vec2d( 1, 0)
up    = v2d.Vec2d( 0, 1)
down  = v2d.Vec2d( 0, -1)



class Ant(object):
    def __init__(self, pos):
        self.pos = pos
        self.dir = None
        self.alive = True
        self.behaviour = [
            BehaviourMove(0.5,self),
            BehaviourDie(0.1,self)
        ]

    def update(self, world):
        for b in self.behaviour:
            if b.isHappen():
                b.execute(world)
                break

        return self.alive





class Behaviour(object):
    def __init__(self, chanceToHappen, object):
        self.chanceToHappen = chanceToHappen
        self.object = object


    def isHappen(self):
        if (random.random() <= self.chanceToHappen):
            return True
        else:
            return False

    def execute(self,world):
        pass

class BehaviourMove(Behaviour):
    def execute(self,world):
        #choose direction
        print "move"
        if self.object.dir is not None:
            self.object.dir = random.choice( [self.object.dir]*5 + [left, right, up, down])
        else:
            self.object.dir = random.choice( [left, right, up, down])

        self.object.pos = self.object.pos + self.object.dir

        if self.object.pos in world:
            world.mark( self.object.pos )
        else:
            self.object.alive = False

class BehaviourDie(Behaviour):
    def execute(self,world):
        print "deadexe"
        self.object.alive = False