__author__ = 'Orion'

import ants
import vector2d
import random
from render.pixel import ColorArray

class World(object):
    def __init__(self, size_x, size_y):
        self.population = []
        self.size_y = size_y
        self.size_x = size_x
        self.arr    = ColorArray(size_x, size_y)

    def start(self):
        while self.population:
            self.update()

    def mark(self, pos):
        color = self.arr.get_pixel( pos.x, pos.y)
        if (color != [0,0,0]).all():
            color *= 1.1
        else:
            color = [64,64,64]
        self.arr.set_pixel( pos.x, pos.y, color)
        self.arr.save(path="screen.png")

    def __contains__(self, pos):
        if pos.x < 0 or pos.x >= self.size_x:
            return False
        elif pos.y < 0 or pos.y >= self.size_y:
            return False
        else:
            return True

    def update(self):
        to_remove = []
        for a in self.population:
            alive = a.update(self)

            if not alive:
                print "die"
                to_remove.append(a)
        for el in to_remove:
            self.population.remove(el)

    def populate(self, nb_ants):
        for i in range(nb_ants):
            x = random.choice( range( self.size_x) )
            y = random.choice( range( self.size_y) )
            self.population.append( ants.Ant( vector2d.Vec2d(x, y) ) )