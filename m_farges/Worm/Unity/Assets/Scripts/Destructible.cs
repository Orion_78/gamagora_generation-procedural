#define DIE_ACTIVATION
//#define DIE_MOVE

//#define FADEINOUT

using UnityEngine;
using System.Collections;
using System.Collections.Generic;



public class Destructible : MonoBehaviour {
	public static List<DestructibleInterface> destructibleInterfacesListenner = new List<DestructibleInterface>();
    public List<DestructibleInterface> privateDestructibleInterfacesListenner = new List<DestructibleInterface>();

	public int MaximumHealth = 100;
	public int damageReduction = 0;
	public int currentHealth;
    public float invulnerableTimeAfterDamage = 5;
    public float forceShieldExplosionPower = 2000;
    public float forceShieldRadius = 10;

    float timeToFadeIn = 3;

    public UnvulnerableMode unvulnerableMode = UnvulnerableMode.none;
    public float blinkInvulnerableSpeed = 0.5f;

    public GameObject forceFieldObject;

    public enum UnvulnerableMode
    {
        none,
        blink,
        forceShield
    }
    


    public GameObject[] dieEffects;
    public GameObject[] getDamageEffects;
    public AudioClip[] dieSoundEffects;
    public AudioClip[] getDamageSoundEffects;

    public GameObject[] dyingEffects;
    public float[] lifeForDyingEffects;

    bool currentlyInvulnerable = false;

 //   public int timeToDie = 2;
	
	
	//public GameObject OtherObjectToDestroy = null;



   

#if DIE_MOVE
    public bool isKinematic;
#endif



	// SPECIAL ASTEROIDES


    public void Awake()
	{
		currentHealth = MaximumHealth;
#if DIE_MOVE
        if (rigidbody)
        isKinematic = rigidbody.isKinematic;
#endif
	}

    void Start()
    {
		if (forceFieldObject) {
						forceFieldObject = Instantiate (forceFieldObject, transform.position, transform.rotation * Quaternion.Euler(new Vector3(0,90,0))) as GameObject;
						forceFieldObject.transform.parent = transform;
				}
        if (dyingEffects != null)
        {
            foreach (GameObject e in dyingEffects)
            {
                e.SetActive(false);
            }
        }
    }


    
    IEnumerator blink()
    {
        currentlyInvulnerable = true;
        float currentTimeBlink = 0;

        while (currentTimeBlink < invulnerableTimeAfterDamage)
        {
            currentTimeBlink += Time.deltaTime;

            Renderer[] rs = GetComponentsInChildren<Renderer>();
            foreach (Renderer r in rs)
                r.enabled = !r.enabled;

            yield return null;
        }

        goVulnerable();
        currentlyInvulnerable = false;
    }

    IEnumerator forceShield()
    {
        currentlyInvulnerable = true;
        
		forceFieldObject.SetActive (true);

        StartCoroutine(scaleOnTimeUp(forceFieldObject));

        float currentTimeBlink = 0;

         while (currentTimeBlink < invulnerableTimeAfterDamage)
         {

             currentTimeBlink += Time.deltaTime;

             Vector3 explosionPos = transform.position;
             Collider[] colliders = Physics.OverlapSphere(explosionPos, forceShieldRadius);
             foreach (Collider hit in colliders)
             {
                 Vector3 dir = (hit.transform.position - transform.position).normalized;

                 if (hit && hit.rigidbody && !hit.rigidbody.isKinematic)
                     hit.rigidbody.AddForce(dir * forceShieldExplosionPower);
             }


             yield return null;
         }

		StartCoroutine (scaleOnTimeDown (forceFieldObject));;
        

    }

    IEnumerator scaleOnTimeUp(GameObject o)
    {
		float forceShieldRadiusMax = 2;

		float totalTime = 0.3f;
        float currentTime = 0;
		float startScale = 0;
		float difToEndScale = 1;
		float endScale = forceShieldRadiusMax * (1+difToEndScale);

		while ( Mathf.Abs(endScale - forceShieldRadiusMax) > 0.1f) {
			currentTime = 0;

			while (currentTime < totalTime) {
				currentTime += Time.deltaTime;

				o.transform.localScale = Vector3.one * Mathf.Lerp (startScale, endScale, currentTime / totalTime );

				yield return null;
			}

			totalTime /= 2;
			difToEndScale /= 2;
			startScale = endScale;
			if (endScale > forceShieldRadiusMax)
				endScale = forceShieldRadiusMax * (1-difToEndScale);
			else
				endScale = forceShieldRadiusMax * (1+difToEndScale);	


		}

		o.transform.localScale = Vector3.one * forceShieldRadiusMax;
    }



	IEnumerator scaleOnTimeDown(GameObject o)
	{
		float totalTime = 1;
		float currentTime = 0;
		float startScale = o.transform.localScale.x;

		float endScale = 0;

		while (currentTime < totalTime) {
			currentTime += Time.deltaTime;
		
			o.transform.localScale = Vector3.one * Mathf.Lerp (startScale, endScale, currentTime / totalTime);
				
			yield return null;
		}
		currentlyInvulnerable = false;
		forceFieldObject.SetActive (false);
	}

    public void goVulnerable()
    {
        
        Renderer[] rs = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in rs)
            r.enabled = true;
   
        renderer.enabled = true;

    //    foreach(DestructibleInterface d in destructibleInterfacesListenner)
    //        d.objetGetVulnerable(this.gameObject);
    }

    public void goUnvulnerable()
    {
        switch (unvulnerableMode)
        {
            case UnvulnerableMode.blink:
                StartCoroutine("blink");
                break;
            case UnvulnerableMode.forceShield:
                StartCoroutine("forceShield");
                break;
        }
    }

	// return true if killed
	public bool getDamage(int dmg)
	{
        if (!currentlyInvulnerable && currentHealth > 0)
        {
            if (dmg > damageReduction)
            {
                currentHealth -= dmg - damageReduction;

            //    if(getDamageSoundEffects.Length != 0)
               //     AudioManager._this.playSoundAtPoint(getDamageSoundEffects[Random.Range(0,getDamageSoundEffects.Length)], transform.position);

                if (getDamageEffects.Length > 0)
                    Instantiate(getDamageEffects[Random.Range(0,getDamageEffects.Length)], transform.position, Random.rotation);
                // print(currentHealth);



                if (currentHealth <= 0)
                {
                    goDeath();
                    return true;
                }
                else
                {
                    foreach (DestructibleInterface d in destructibleInterfacesListenner)
                        d.objectTakeDamage(this.gameObject,dmg);

                    foreach (DestructibleInterface d in privateDestructibleInterfacesListenner)
                        d.objectTakeDamage(this.gameObject,dmg);

                    goUnvulnerable();

                    for (int i = 0; i< lifeForDyingEffects.Length; i++)
                    {
                        if (lifeForDyingEffects[i] > currentHealth)
                        {
                            dyingEffects[i].SetActive(true);
                        }
                    }

                }
            }
        }
        

        return false;
	}
	
	public virtual void goDeath()
	{
        if (gameObject.activeSelf)
        {
            //	print(this.name + " is dead");

            // Die Animation

            foreach (GameObject anim in dieEffects)
            {
                Quaternion direction;
                if (rigidbody && rigidbody.velocity != Vector3.zero)
                {
                    direction = Quaternion.LookRotation(-rigidbody.velocity);
                }
                else
                {
                    direction = Quaternion.LookRotation(transform.forward);
                }
                Instantiate(anim, transform.position, direction);
                //   ob.transform.parent = transform.parent;

                // Is there another object we should destroy when this object is killed?
                //Destroy(ob, timeToDie);
            }

       //     if (dieSoundEffects.Length > 0 )
        //        AudioManager._this.playSoundAtPoint(dieSoundEffects[Random.Range(0, dieSoundEffects.Length)], transform.position);



            goDeathNoAnimation();
        }
		
	}

    public void goDeathNoAnimation()
    {
        if (gameObject.activeSelf)
        {

            currentHealth = 0;
            foreach (DestructibleInterface d in destructibleInterfacesListenner)
            {
                d.OnDeath(gameObject);
            }

            foreach (DestructibleInterface d in privateDestructibleInterfacesListenner)
            {
                d.OnDeath(gameObject);
            }

            foreach (Transform child in transform)
            {
                if (child.gameObject.tag == "ParticleToClean")
                {
                    Destroy(child.gameObject);
                }
            }

            justKillMe();
        }
    }

 

    public void justKillMe()
    {
        /* 
            * MOVE
           */
#if DIE_MOVE
        renderer.enabled = false;
        transform.position = new Vector3(0, 0, 0);

        

        if (rigidbody != null)
        {
           
            rigidbody.isKinematic = true;
        }

        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = false;
        }
        
#elif DIE_ACTIVATION
        gameObject.SetActive(false);
#elif
		Destroy(gameObject);
#endif

        foreach (GameObject e in dyingEffects)
        {
            e.SetActive(false);
        }

		StopAllCoroutines ();
		if (forceFieldObject)
			forceFieldObject.SetActive (false);

       
    }

    public virtual void goAlive()
    {
		if (forceFieldObject)
			forceFieldObject.SetActive (false);
		/*
		 * MOVE
		 */
        foreach (DestructibleInterface d in privateDestructibleInterfacesListenner)
        {
            d.OnAlive(gameObject);
        }
        foreach (DestructibleInterface d in destructibleInterfacesListenner)
        {
            d.OnAlive(gameObject);
        }

        currentHealth = MaximumHealth;

        if (dyingEffects != null)
        {
            foreach (GameObject e in dyingEffects)
            {
                e.SetActive(false);
            }
        }


#if DIE_MOVE
        renderer.enabled = true;
       
        if (rigidbody != null)
            rigidbody.isKinematic = isKinematic;

        foreach (Collider c in GetComponents<Collider>())
        {
            c.enabled = true;
        }
#elif DIE_ACTIVATION
        gameObject.SetActive(true);
#endif

#if FADEINOUT
        StartCoroutine(fadeIn());
#endif

    }

    IEnumerator fadeIn()
    {
        Color c = renderer.material.color;
        c.a = 0;
        renderer.material.color = c;
        
        float currentTime = 0;

        while (currentTime < timeToFadeIn)
        {
            currentTime += Time.deltaTime;   
            yield return null;
            c.a = Mathf.Lerp(0,1, currentTime/timeToFadeIn );
            renderer.material.color = c;
        }
        c.a = 1;
        renderer.material.color = c;
    }

   /* void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, forceShieldRadius);
        
    }*/
}
