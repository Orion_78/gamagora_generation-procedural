﻿using UnityEngine;
using System.Collections;

public interface DestructibleInterface {

 //   void objetGetVulnerable(GameObject theGameObject) { }
    void objectTakeDamage(GameObject theGameObject, int dmg);


    void OnDeath(GameObject deadObject);
    void OnAlive(GameObject aliveObject);


}