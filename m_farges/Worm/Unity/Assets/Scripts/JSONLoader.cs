﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

using UnityEditor;


using System;
using System.IO;
using System.IO.Compression;
using System.Text;



public class JSONLoader : MonoBehaviour
{
    public static JSONLoader _this;



    public Material material;

    public PoolManagerBase pool;
    public int speedSpawn = 300;

    public bool loadVoxel;
    public float sizeScale = 1;

    public bool autoreload;
    public string fileName;
    // JSONNode textData;
    StreamReader theReader;

    GameObject root;

    public GameObject getRoot()
    {
        return root;
    }

    List<Destructible> voxelCreated = new List<Destructible>();

    // Use this for initialization
    void Awake()
    {
        _this = this;
        root = new GameObject("root");
           reloadFile();


        //  StartCoroutine("loadVoxels");
    }

    public void reloadFile()
    {
        // Clean scene
        foreach (Destructible d in voxelCreated)
        {
            d.transform.parent = pool.transform.GetChild(0).transform;
            d.goDeathNoAnimation();
        }
        voxelCreated.Clear();

        foreach (Transform child in root.transform)
        {
            Destroy(child.gameObject);

        }
        //   pool.transform.localScale = Vector3.one;


        theReader = new StreamReader("Assets/Resources/" + fileName + ".json", Encoding.Default);

        if (!loadVoxel)
        {
            loadMesh();
        }
        else
        {
            root.transform.localScale = Vector3.one;
        }
    }



    void Update()
    {
        if (loadVoxel)
        {
            for (int i = 0; i < speedSpawn; i++)
            {
                loadNextVoxel();

                /*  if (indexCreate < textData[0].Count)
                  {
                      GameObject v = pool.pop(0);

                      Vector3 pos = new Vector3(int.Parse(textData[0][indexCreate]["position"][0].Value), int.Parse(textData[0][indexCreate]["position"][1].Value), int.Parse(textData[0][indexCreate]["position"][2].Value));
                      indexCreate++;
                      v.transform.position = pos;

                      voxelCreated.Add(v.GetComponent<Destructible>());
                  }*/
            }
        }

    }

    void loadMesh()
    {

        String line = theReader.ReadLine();

        // READ File
        /* Old format*/
        int size = int.Parse(line);
        float[, ,] voxels = new float[size, size, size];

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int z = 0; z < size; z++)
                {
                    line = theReader.ReadLine();

                    try
                    {
                        voxels[x, y, z] = float.Parse(line);
                    }
                    catch (ArgumentNullException e)
                    {

                    }

                }
            }

        }
        

        /*
        int size = int.Parse(line);

        List<int> numbers = new List<int>();
        do
        {
            try
            {
                line = theReader.ReadLine();
                numbers.Add(int.Parse(line));
            }
            catch (ArgumentNullException e)
            {

            }
        } while (line != null);

        
        float[, ,] voxels = new float[size, size, size];

        int count = 0;
        bool minus = true;
        int index = 0;

        for (int x = 0; x < size; x++)
        {
            for (int y = 0; y < size; y++)
            {
                for (int z = 0; z < size; z++)
                {
                    if (count > 0)
                    {
                        if (minus)
                        {
                            voxels[x, y, z] = -1;
                        }
                        else
                        {
                            voxels[x, y, z] = 1;
                        }
                        count--;
                    }
                    else
                    {
                        minus = !minus;
                        count = numbers[index];
                        index++;
                    }

                    

                }
            }

        }

        */

        theReader.Close();


        root.transform.localScale = Vector3.one;
        List<Mesh> meshs = MarchingCubes.CreateMesh(voxels);

        foreach (Mesh m in meshs)
        {
            //The diffuse shader wants uvs so just fill with a empty array, there not actually used
            m.uv = new Vector2[m.vertices.Length];
            m.RecalculateNormals();

            GameObject m_mesh = new GameObject("Mesh");
            m_mesh.transform.parent = root.transform;
            m_mesh.AddComponent<MeshFilter>();
            m_mesh.AddComponent<MeshRenderer>();
            m_mesh.renderer.material = material;
            m_mesh.GetComponent<MeshFilter>().mesh = m;
            //Center mesh
            m_mesh.transform.localPosition = new Vector3(-size / 2, -size / 2, -size / 2);
        }


        root.transform.localScale = new Vector3(sizeScale, sizeScale, sizeScale);

    }

    void OnDestroy()
    {
        if (theReader.BaseStream != null)
            theReader.Close();
    }


    void loadNextVoxel()
    {
        // using (theReader)
        //  {




        if (theReader.BaseStream != null)
        {
            string line = theReader.ReadLine();
            if (line != null)
            {

                // Do whatever you need to do with the text line, it's a string now
                // In this example, I split it into arguments based on comma
                // deliniators, then send that array to DoStuff()
                string[] entries = line.Split(' ');
                GameObject v = pool.pop(0);
                v.transform.parent = root.transform;
                v.transform.localScale = new Vector3(sizeScale, sizeScale, sizeScale);
                Vector3 pos = new Vector3(int.Parse(entries[0]) * sizeScale, int.Parse(entries[1]) * sizeScale, int.Parse(entries[2]) * sizeScale);

               

                v.transform.position = pos;

                voxelCreated.Add(v.GetComponent<Destructible>());
                //if (entries.Length > 0)
                //  DoStuff(entries);
            }
            else
            {
                theReader.Close();
                //     if (doResize)
                //         pool.transform.localScale = new Vector3(size, size, size);
            }

        }
        //  }
    }


      void OnGUI()
      {
          if (GUI.Button(new Rect(10,10,100,100), "Stop/Reload"))
          {
              if (theReader.BaseStream != null)
                  theReader.Close();
              else
                  reloadFile();
          }
      }

}


public class DetectChanges : AssetPostprocessor
{
    


    static void OnPostprocessAllAssets(
        string[] importedAssets, 
        string[] deletedAssets,
        string[] movedAssets,
        string[] movedFromAssetPaths)
    {
        if (JSONLoader._this.autoreload && Application.isPlaying)
            JSONLoader._this.reloadFile();

        /*
        foreach (string str in importedAssets)
        {
            Debug.Log("Reimported Asset: " + str);
            string[] splitStr = str.Split('/', '.');

            string folder = splitStr[splitStr.Length - 3];
            string fileName = splitStr[splitStr.Length - 2];
            string extension = splitStr[splitStr.Length - 1];
            Debug.Log("File name: " + fileName);
            Debug.Log("File type: " + extension);
        }

        foreach (string str in deletedAssets)
            Debug.Log("Deleted Asset: " + str);

        for (int i = 0; i < movedAssets.Length; i++)
            Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);*/
    }
}







/*
    * TODO = No load from now
    * Load JSON
   //
    * 
    * 
       TextAsset file = (TextAsset)Resources.Load(fileName);

       if (file == null)
       {
           Debug.Log("file not loaded");
       }
       else
       {
           Debug.Log("File Exists");
           Debug.Log(file.text);
           //   string decompressed = Ionic.Zlib.GZipStream.UncompressString(file.bytes);
           //string jsonText = System.Text.ASCIIEncoding.ASCII.GetString(decompressed);

           //Debug.Log(decompressed);
           //textData = JSON.Parse(file.text);
           //Debug.Log(textData);
       }
    * 
    */

/*public static string Decompress(string s)
{
    var bytes = Convert.FromBase64String(s);
    using (var msi = new MemoryStream(bytes))
    using (var mso = new MemoryStream())
    {
        using (var gs = new GZipStream(msi, CompressionMode.Decompress))
        {
            gs.CopyTo(mso);
        }
        return Encoding.Unicode.GetString(mso.ToArray());
    }
}*/

//int indexCreate = 0;

/* IEnumerator loadVoxels()
 {
     while (true)
     {
         for (int i = 0; i < 300; i++)
         {
             loadNextVoxel();
/*
             if (indexCreate < textData[0].Count)
             {
                 GameObject v = pool.pop(0);

                 Vector3 pos = new Vector3(int.Parse(textData[0][indexCreate]["position"][0].Value), int.Parse(textData[0][indexCreate]["position"][1].Value), int.Parse(textData[0][indexCreate]["position"][2].Value));
                 indexCreate++;
                 v.transform.position = pos;

                 voxelCreated.Add(v.GetComponent<Destructible>());
             }*
         }
         yield return null;
     }
 }*/