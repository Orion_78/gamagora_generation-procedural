﻿using UnityEngine;
using System.Collections;



public class SpaceController : MonoBehaviour
{
    public float acceleration = 10;
    public float accelerationBoost = 20;
    public float maxVelocity = 10;
    public float maxVelocityBoost = 20;
    public float drag = 1;

    public float accelerationRotation = 10;

   

    public float mobileRotationInputSensibilite;
    public float mobileRotationDeadZone;
    
    public bool doRotation = true;

    public Texture buttonTexture;

    Vector2 oldPosition;

    float newAccel;
    float vitesseCourant;

    void Awake()
    {
        Input.gyro.enabled = true;

      
    }


    // Update is called once per frame
    void Update()
    {
        if (rigidbody != null)
        {
            // FORWARD CONTROL
            vitesseCourant = rigidbody.velocity.magnitude;

            //rigidbody.velocity = accelerationBoost * transform.forward;

            if (Input.GetButton("Fire2") || Input.touchCount == 2 )
            {
                newAccel = Mathf.Clamp(maxVelocityBoost - vitesseCourant, -drag * Time.deltaTime, accelerationBoost * Time.deltaTime);
            }
            else
            {
                newAccel = Mathf.Clamp(maxVelocity - vitesseCourant, -drag * Time.deltaTime, acceleration * Time.deltaTime);
            }

            rigidbody.velocity = (vitesseCourant + newAccel) * transform.forward;
        }

        if (doRotation)
        {

            // ROTATION CONTROL
#if UNITY_IPHONE || UNITY_ANDROID
            if (Input.touchCount > 0)
            {
                if (Input.touches[0].phase == TouchPhase.Began)
                {
                    oldPosition = Input.touches[0].position;
                }
                if (Input.touches[0].phase == TouchPhase.Ended)
                {
                    if (Input.touchCount > 1)
                        oldPosition = Input.touches[1].position;
                }


                Vector2 movement = Input.touches[0].position - oldPosition;

                Vector2 dir = movement.normalized;

                float mag = (movement.magnitude - mobileRotationDeadZone) / mobileRotationInputSensibilite;
                if (mag < 0)
                {
                    mag = 0;
                }

                mag = Mathf.Clamp(mag, 0, 1);

                transform.Rotate(Vector3.up, dir.x * mag * accelerationRotation * Time.deltaTime);
                transform.Rotate(Vector3.left, dir.y * mag * accelerationRotation * Time.deltaTime);
            }
            else
            {
                oldPosition = new Vector3(-100, -100);
            }
#endif

            transform.Rotate(Vector3.up, Input.GetAxis("Horizontal") * accelerationRotation * Time.deltaTime);
            transform.Rotate(Vector3.left, Input.GetAxis("Vertical") * accelerationRotation * Time.deltaTime);
        }
        else
        {
            oldPosition = new Vector3(-100, -100);
        }

     
        
//#endif

    }

#if UNITY_IPHONE || UNITY_ANDROID
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(oldPosition.x - 32, Screen.height - oldPosition.y - 32, 64, 64), buttonTexture);
    }
#endif


   
    
}
