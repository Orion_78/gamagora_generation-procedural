﻿using UnityEngine;
using System.Collections.Generic;

public class PoolManagerBase : MonoBehaviour, DestructibleInterface
{
	protected List<Stack<GameObject>> pools = new List<Stack<GameObject>>();

	[HideInInspector]
	public List<GameObject> roots;
	
	public GameObject objectToInstantiate;
	
	public int numberToPreInstantiate;
	
	public List<int> pallierDeVie;
	
	public float coefSizeHealh = 1;
	
	public int currentSpawned = 0;

	protected int id = 0;

    protected virtual void Start()
    {
        for (int i = 0; i < pallierDeVie.Count; i++)
        {
            GameObject root = new GameObject("Etage " + pallierDeVie[i]);
            root.transform.parent = transform;
            root.transform.localScale = Vector3.one * (pallierDeVie[i] * coefSizeHealh);
            root.AddComponent<Destructible>().MaximumHealth = pallierDeVie[i];

            roots.Add(root);
            pools.Add(new Stack<GameObject>());

            // MOVE
            for (int j = 0; j < numberToPreInstantiate; j++)
            {
                GameObject o = generate(i);
                o.GetComponent<Destructible>().justKillMe();

                pools[i].Push(o);
                
            }
        }
    }

    protected virtual GameObject generate(int etage)
    {
        GameObject toPop = Instantiate(objectToInstantiate) as GameObject;
        toPop.name += " " + id;
        id++;
        toPop.transform.parent = roots[etage].transform;
        toPop.transform.localScale = Vector3.one;

        toPop.GetComponent<Destructible>().privateDestructibleInterfacesListenner.Add(this);
        toPop.GetComponent<Destructible>().MaximumHealth = roots[etage].GetComponent<Destructible>().MaximumHealth;
        //			toPop.GetComponent<Asteroides>().etage = etage;
        toPop.AddComponent<PoolManagerDebug>().etage = etage;

        return toPop;
    }

    public GameObject pop(int etage)
    {
        GameObject toPop;
        if (pools[etage].Count > 0)
        {
            toPop = pools[etage].Pop();
            
        }
        else
        {
            toPop = generate(etage);
            toPop.name += "Extra";
        }

        toPop.GetComponent<Destructible>().goAlive();

       
        return toPop;
    }

    public void OnDeath(GameObject deadObject) 
    {
      //  Debug.Log(deadObject.name + " destroyed");
        // MOVE
        //Debug.Log (pools [etage].Count);
        pools[deadObject.GetComponent<PoolManagerDebug>().etage].Push(deadObject);
        //Debug.Log (pools [etage].Count);

        currentSpawned--;
    }

    public void OnAlive(GameObject aliveObject) 
    {
        currentSpawned++;
    }

    public void objectTakeDamage(GameObject theGameObject, int dmg)
    { }
	
}
