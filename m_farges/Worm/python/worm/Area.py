__author__ = 'Orion'
import numpy
import json
import gzip
import codecs
import math
import Utils
import numpy as np
#import mcubes

class area(object):
    # 0 : full
    # 1 : empty

    def __init__(self, size):
        self.size = size
        self.tab = numpy.zeros((size,size,size))

        #make a sphere
        for x in range(size):
            for y in range(size):
                for z in range(size):
                    if ( ( math.pow( x - size/2 ,2) + math.pow(y-size/2,2) + math.pow(z-size/2,2) ) > pow(size/2,2) ):
                        self.empty(x,y,z)

    def empty(self, x,y,z):
        try:
            self.tab[x, y, z] = "1"
        except IndexError:
            pass

    def cleanAreaAround(self, posRound, size):
        a = posRound.x - size/2
        b = posRound.x + size/2
        for x in range( int(a) , int(b)  ):
            for y in range( int(posRound.y - size/2 ), int(posRound.y + size/2) ):
                for z in range( int(posRound.z - size/2), int(posRound.z + size/2) ):
                    if ( ( math.pow(x-posRound.x,2) + math.pow(y-posRound.y,2) + math.pow(z-posRound.z,2) ) <= pow(size/2,2) ):
                        self.empty(x,y,z)

    def contain(self, voxel):

        try:
            self.tab[voxel.x,voxel.y,voxel.z]
            return True
        except IndexError:
            return False

        #if voxel.x >= 0 and voxel.x < self.size and voxel.y >= 0 and voxel.y < self.size and voxel.z >= 0 and voxel.z < self.size:
         #   return True
        #else:
            #return False

    def __repr__(self):
        text = ''
        for x in range(self.size):
            for y in range(self.size):
                for z in range(self.size):
                    text += "%s" % (self.tab[x,y,z]) + " "
                text += '\n'
        return text

    def serializePixelsPosition(self, path):
        print "begin serialize"
        f = open(path, 'w')

        for x in range(self.size):
            for y in range(self.size):
                for z in range(self.size):
                    if self.tab[x,y,z] == 0 and \
                            not (x+1 < self.size and y+1 < self.size and z+1 < self.size and \
                                x-1 > 0 and y-1 > 0 and z-1 > 0 and \
                            (self.tab[x,y,z+1]) == 0 and (self.tab[x,y+1,z]) == 0 and (self.tab[x+1,y,z]) == 0 and \
                            (self.tab[x,y,z-1]) == 0 and (self.tab[x,y-1,z]) == 0 and (self.tab[x-1,y,z]) == 0 ):
                        line = "%s %s %s\n" % (x,y,z)
                        f.write(line)

    def serializePolygone(self, path):
        print "begin serialize"
        f = open(path, 'w')
        f.write("%s\n" % (self.size) )


        minus = True
        count = 0
        for x in range(self.size):
            for y in range(self.size):
                for z in range(self.size):
                    if self.tab[x,y,z] == 0 and \
                            not (x+1 < self.size and y+1 < self.size and z+1 < self.size and \
                                x-1 > 0 and y-1 > 0 and z-1 > 0 and \
                            (self.tab[x,y,z+1]) == 0 and (self.tab[x,y+1,z]) == 0 and (self.tab[x+1,y,z]) == 0 and \
                            (self.tab[x,y,z-1]) == 0 and (self.tab[x,y-1,z]) == 0 and (self.tab[x-1,y,z]) == 0 ):
                       #
                       #  if minus:
                       #     f.write("%s\n" % count)
                       #     count = 1
                       # else:
                       #     count = count + 1
                        f.write("1\n")

                    else:
                        #if not minus:
                         #   f.write("%s\n" % count)
                          #  count = 1
                        #else:
                          #  count = count + 1
                        f.write("-1\n")





    def serializeJsonTest(self):
        playlist = {}
        playlist["voxels"] = []

        for x in range(self.size):
            for y in range(self.size):
                for z in range(self.size):
                    if self.tab[x,y,z] == 1:
                        vox = {}
                        vox["position"] = []
                        #vox = [x, y, z]
                        vox["position"].append(x)
                        vox["position"].append(y)
                        vox["position"].append(z)
                        #vox.append(x)
                        #vox.append(y)
                        #vox.append(z)
                        playlist["voxels"].append(vox)
                        #playlist.append()["v"] = vox

       # with gzip.GzipFile("Test.json", 'w') as outfile:
        #    outfile.write(json.dumps(playlist) + '\n')
        with codecs.open('Test.json', 'w', 'utf-8') as f:
            json.dump(playlist, f, indent=4)