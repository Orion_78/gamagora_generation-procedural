__author__ = 'Orion'

import random
import Worm
import math
import vector

class WormNode(object):
    def __init__(self, voxel, size, arena):
        print "begin worm Node"
        arena.cleanAreaAround(voxel,size)
        ra = (int)(arena.size * 0.3)
        print ra
        for i in range(ra): #random.randrange(10,50)
            randSize = random.randrange(3,10)
            w = Worm.worm(voxel,randSize,arena)
            w.dir = vector.Vector(random.random() * 2 - 1,random.random()  * 2 - 1,random.random()  * 2 - 1)
            w.start()


