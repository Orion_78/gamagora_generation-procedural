__author__ = 'Orion'

class voxel(object):
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def __add__(self, v):
        return voxel(self.x+v.x,self.y+v.y, self.z+v.z)

    def __repr__(self):
        return "%s %s %s" % (self.x,self.y,self.z)