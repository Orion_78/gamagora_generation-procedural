__author__ = 'Orion'
import Utils
import random
import vector
import math

class worm(object):
    def __init__(self, voxel, size, arena):
        self.pos = voxel
        self.size = size
       # self.size = 1
        self.posRound = self.pos
        self.dir = vector.Vector(1,0,0)
        self.up = vector.Vector(0,1,0)
        self.right = vector.Vector(0,0,1)
        self.arena = arena
        self.wormsSon = []
        self.wormsSon.append(self)
        self.move = 0

    def start(self):
        print "worm start"
        self.wormsSon.append(self)
        while len(self.wormsSon) > 0:

            for w in self.wormsSon:
                if not w.update():
                    self.wormsSon.remove(w)



    def update(self):
        self.move = self.move + 1

        if self.arena.contain(self.posRound) and self.move < self.arena.size*2:
            self.arena.cleanAreaAround(self.posRound,self.size)
            self.oneMove()
            return True
        else:
            return False






    def oneMove(self):

        r = random.random()
        #chance de tourner
        if (r < 0.2):
            self.randomTurnDefault()


        r = random.random()
        #create new worm
     #   if (r < 0.1):
      #      print "new worm"
       #     r = random.randrange(3,5) #int(self.size*1.5)+1
        #    w = worm(self.posRound,r,self.arena)
         #   w.dir = self.dir
          #  angle = random.randrange(1,2)
           # angle = math.pi/angle
            #w.randomTurn(angle)
            #self.wormsSon.append(w)



        #Move forward
        self.pos = self.pos + self.dir
        self.posRound = Utils.voxel(round(self.pos.x), round(self.pos.y), round(self.pos.z))
        #print self.dir

    def randomTurnDefault(self):
        range = math.pi/2
        self.randomTurn(random.random() * range * 2 - range )


    def randomTurn(self, t):
        r = random.random()

        #turn up/down
        if (r >= 0.5):
                self.dir = self.dir.rotate( self.right, t )
                self.up = self.up.rotate( self.right,  t )

        #turn right/left
        else:
                self.dir = self.dir.rotate( self.up, t )
                self.right = self.right.rotate( self.up,  t )


