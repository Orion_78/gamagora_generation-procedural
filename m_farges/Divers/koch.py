import Arbreframwork
import turtleframwork

# KOCH #################################################################

class RuleKoch(Arbreframwork.Rule):
    cmd_name = 'F'

    def apply(self, cmd):
        if not self.match(cmd):
            return [cmd]
        return [Arbreframwork.Cmd('F',cmd.param) , Arbreframwork.Cmd('+',60) , Arbreframwork.Cmd('F',cmd.param) , \
               Arbreframwork.Cmd('-',120)  , Arbreframwork.Cmd('F',cmd.param) , Arbreframwork.Cmd('+',60) , Arbreframwork.Cmd('F',cmd.param)]


# EXEMPLE ##############################################################
if __name__ == "__main__":
    turtleframwork.turtle.delay(0)

    # place turtle at a good place
    turtleframwork.turtle.up()
    turtleframwork.turtle.setpos(-400,0)
    turtleframwork.turtle.down()



    default_ruleManager = Arbreframwork.RuleManager( [ RuleKoch() ] )
    default_ruleManager.apply( 2, [Arbreframwork.Cmd('F',3)] )
    default_renderer = Arbreframwork.Renderer( [ turtleframwork.ForwardCmd(), turtleframwork.RightCmd(), turtleframwork.LeftCmd() ] )
    default_renderer.draw( default_ruleManager.apply( 5, [Arbreframwork.Cmd('F',3)] ) )
    # r.drawing_cmds = []
    #draw(makeKoch(2))

    raw_input()