import turtle
import Arbreframwork

class ForwardCmd(Arbreframwork.DrawingCmd):
    cmd_name = 'F'

    def execute(self, context, cmd):
        if not self.match(cmd):
            return
      #  dist = cmd.param
      #   turtle.begin_fill()
      #   turtle.left(90)
      #   turtle.forward(cmd.param/2/2)
      #   turtle.right(100)
      #   turtle.forward(cmd.param)
      #   turtle.right(80)
      #   turtle.forward(cmd.param/3)
      #   turtle.right(80)
      #   turtle.forward(cmd.param)
      #   turtle.right(100)
      #   turtle.forward(cmd.param/2/2)
      #   turtle.right(90)
      #   turtle.end_fill()
        turtle.width(cmd.param/2)
        turtle.forward(cmd.param)
        turtle.width(1)



class LeftCmd(Arbreframwork.DrawingCmd):
    cmd_name = '+'

    def execute(self, context, cmd):
        if not self.match(cmd):
            return
      #  dist = cmd.param
        turtle.left(cmd.param)

class RightCmd(Arbreframwork.DrawingCmd):
    cmd_name = '-'

    def execute(self, context, cmd):
        if not self.match(cmd):
            return
      #  dist = cmd.param
        turtle.right(cmd.param)


class PushCmd(Arbreframwork.DrawingCmd):
    cmd_name = '['

    def __init__(self):
        self.turtlePos = []
        self.turtleHeading = []

    def execute(self, context, cmd):
        if not self.match(cmd):
            return
        self.turtlePos = [turtle.pos()] + self.turtlePos
        self.turtleHeading = [turtle.heading()] + self.turtleHeading

class PopCmd(Arbreframwork.DrawingCmd):
    cmd_name = ']'

    def __init__(self, push):
        self.pushCmd = push

    def execute(self, context, cmd):
        if not self.match(cmd):
            return
        turtle.setpos(self.pushCmd.turtlePos[0])
        turtle.setheading(self.pushCmd.turtleHeading[0])
        self.pushCmd.turtlePos = self.pushCmd.turtlePos[1:]
        self.pushCmd.turtleHeading = self.pushCmd.turtleHeading[1:]