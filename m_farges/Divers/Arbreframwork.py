class Cmd(object):
    def __init__(self, name, param):
        self.name = name
        self.param = param

    def __repr__(self):
        return "%s(%s)" % (self.name, self.param)

class Rule(object):
    def match(self, cmd):
        if cmd.name == self.cmd_name:
            return True
        else:
            return False

    def apply(self, cmd):
        pass

class DrawingCmd(object):
    def match(self, cmd):
        return cmd.name == self.cmd_name

    def execute(self, context, cmd):
        pass

class RuleManager(object):
    def __init__(self, rules_cmds):
        self.rules_cmds = rules_cmds

    def apply(self, iteration, cmds):
        for i in range(iteration):
            temp = []
            for rule in self.rules_cmds:
                for c in cmds:
                    temp.extend(rule.apply(c))
            cmds = temp
        return cmds

class Renderer(object):

    def __init__(self, drawing_cmds):
        self.drawing_cmds = drawing_cmds

    def draw(self, cmds, context = {} ):
        for cmd in cmds:
            for drawing_cmd in self.drawing_cmds:
                drawing_cmd.execute( context, cmd )


