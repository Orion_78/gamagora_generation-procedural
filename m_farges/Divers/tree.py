import turtleframwork
import Arbreframwork

# TREE #################################################################

class RuleTree(Arbreframwork.Rule):
    cmd_name = 'F'

    def apply(self, cmd):
        if not self.match(cmd):
            return [cmd]
        return [ Arbreframwork.Cmd('F', cmd.param), Arbreframwork.Cmd('[', None), Arbreframwork.Cmd('+', 60), Arbreframwork.Cmd('F',cmd.param/3), Arbreframwork.Cmd(']', None),
                 Arbreframwork.Cmd('F', cmd.param/2), Arbreframwork.Cmd('[', None), Arbreframwork.Cmd('-', 60), Arbreframwork.Cmd('F',cmd.param/3), Arbreframwork.Cmd(']', None), Arbreframwork.Cmd('F', cmd.param/2)]




# EXEMPLE ##############################################################
if __name__ == "__main__":
    #turtleframwork.turtle.delay(100)

    default_ruleManager = Arbreframwork.RuleManager( [ RuleTree() ] )
    pushCmd = turtleframwork.PushCmd()
    default_renderer = Arbreframwork.Renderer( [ turtleframwork.ForwardCmd(), turtleframwork.RightCmd(), turtleframwork.LeftCmd(), pushCmd, turtleframwork.PopCmd(pushCmd) ] )

    # place turtle at a good place
    turtleframwork.turtle.left(90)
    turtleframwork.turtle.up()
    turtleframwork.turtle.setpos(0,-300)
    turtleframwork.turtle.down()
    #default_renderer.draw( [Arbreframwork.Cmd('F',50)] )
    default_renderer.draw( default_ruleManager.apply(3,[Arbreframwork.Cmd('F',50)]) )

    # r.drawing_cmds = []
    #draw(makeKoch(2))

    raw_input()