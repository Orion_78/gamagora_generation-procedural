from m_farges.Noise import vector2d

__author__ = 'Orion'

import random
from render import pyglet_lines
import time

xSize = 600

class Noise1D(object):

    def makeNoise(self, numberPoints, amplitude):
        controlPoints = []
        numberPoints = numberPoints-1
        for i in range(0,numberPoints+1):
            controlPoints.append(vector2d.Vec2d(  ( (i / float(numberPoints)) * xSize),random.randint(0,amplitude)))

        points = []

        for i in range(0,xSize):
            points.append(self.interpolation(controlPoints,i))

        return points


    def interpolation(self, controlPoints, position):
        t = 0
        for i in range(0,len(controlPoints)):
            if (position < controlPoints[i][0]):

                a = controlPoints[i-1][0]
                b = controlPoints[i][0]

                # t indice
                t = (position-a)/(b-a)
                # t interpolation
                t = 3*t*t - 2*t*t*t
                # t hauteur
                t = controlPoints[i-1][1] + t * ( controlPoints[i][1] - controlPoints[i-1][1] )

                break

        return vector2d.Vec2d( position, t )


# TEST ##############################################
if __name__ == "__main__":
    n = Noise1D()
    pyglet_lines.set_point(n.makeNoise(20,500))
    pyglet_lines.run()
    time.sleep(5)