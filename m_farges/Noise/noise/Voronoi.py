import grid
import vector2d
import random
from render import pixel

__author__ = 'Orion'


class Voronoi(object):

    def __init__(self, nbPointMax, sizeCase, sizeTotal):
        self.grid = grid.Grid(sizeCase)
        self.sizeCase = sizeCase
        self.sizeTotal = sizeTotal

        for x in range(sizeTotal/sizeCase):
            for y in range(sizeTotal/sizeCase):
                #create point in case
                for i in range(random.randrange(1,nbPointMax)):
                    pt = vector2d.Vec2d(
                        random.randrange(x*sizeCase,(x+1)*sizeCase) ,
                        random.randrange(y*sizeCase,(y+1)*sizeCase) )
                    #pt = vector2d.Vec2d(x,y)
                    self.grid.add_feature_point(pt)

    def buildMap(self, path):
        ca = pixel.ColorArray(self.sizeTotal, self.sizeTotal)

#        for c in self.grid.cases.values():

 #           color = 255
  #          ca.set_pixel( c.coord[0] * self.sizeCase, c.coord[1] * self.sizeCase, color )

        for x in range(self.sizeTotal):
            for y in range(self.sizeTotal):
                color = self.interpolation2(vector2d.Vec2d(x,y) ) * 255
                ca.set_pixel( x, y, color  )

        ca.save(path=path)


    def interpolation1(self, coord):
        totalDistance = 0
        totalPoint    = 0

        for c in self.grid.neighbours(self.grid.locate(coord)):
            for d in c.distancesToPoints(pt=coord):
                totalDistance += d[1] / (self.sizeCase*2)
            totalPoint += len(c.data)

        if totalPoint == 0:
            return 1
        else:
#        print totalDistance / totalPoint
            return totalDistance / totalPoint

    def interpolation2(self, coord):
        minDistances = 999999
        maxDistances = (self.sizeCase*2)


        for c in self.grid.neighbours(self.grid.locate(coord)):
            for d in c.distancesToPoints(pt=coord):
                if minDistances > d[1]:
                    minDistances = d[1]



        return minDistances/maxDistances



# EXEMPLE ##############################################################
if __name__ == "__main__":
    v = Voronoi(3,10,500)
    v.buildMap("Voronoi.png")
    print v.grid