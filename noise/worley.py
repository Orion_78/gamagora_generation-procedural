# compute_value :: pt, [( pt, distance ) ] -> (pt, value )
from m_farges.Noise import vector2d as v2d


def compute_value(p, closest_points):
    p2, distance = closest_points[0]
    max_distance = closest_points[-1][1]
    return p, (max_distance - distance) / float(max_distance)

# worley :: [ pt ], grid -> [ (pt, value) ]
def worley( pts, grid ):
    result = []
    for p in pts:
        case  = grid.locate(p)
        cases = grid.neighbours(case)
        closest_pts = []
        for c in cases:
            closest_pts.extend( c.closest_points( p ) )
        closest_pts.sort(key=lambda v : v[1])
        np, value = compute_value(p, closest_pts)
        result.append((np, value))
    #==
    return result

## Test
import random

from m_farges.Noise.grid import Grid


def get_feature_points(nb_min, nb_max, min_coord, max_coord):
    result = []
    nb = random.randint(nb_min, nb_max)
    for i in range(nb):
        min_x = min_coord[0]
        max_x = max_coord[0]
        x = int( min_x + (random.random() * (max_x - min_x)) )
        min_y = min_coord[1]
        max_y = max_coord[1]
        y = int( min_y + (random.random() * (max_y - min_y)) )
        result.append( v2d.Vec2d(x,y) )
    return result

def build_grid(size_x, size_y, case_size):
    grid = Grid( case_size )
    min_pt_per_case = 1
    max_pt_per_case = 9
    #==
    nb_case_x = (size_x / case_size) 
    nb_case_y = (size_y / case_size) 
    for x in range(nb_case_x):
        for y in range(nb_case_y):
            min_coord = x*case_size, y*case_size
            max_coord = min_coord[0]+case_size-1, min_coord[1]+case_size-1
            feature_pts = get_feature_points( min_pt_per_case, 
                                              max_pt_per_case,
                                              min_coord, 
                                              max_coord
                                              )
            for fp in feature_pts:
                grid.add_feature_point( fp)
    return grid

def build_pts(size_x, size_y):
    pts = []
    for i in range(size_x):
        for j in range(size_y):
            pts.append( v2d.Vec2d( i, j ))
    return pts

from render import pixel

def main(size_x, size_y, case_size):
    g    = build_grid(size_x, size_y, case_size)
    pts  = build_pts(size_x, size_y)
    npts = worley(pts, g)
    arr  = pixel.ColorArray(size_x,size_y)
    for p in npts : 
        coord = p[0]
        val   = p[1] * 255
        arr.set_pixel( coord.x, coord.y, val)
    #===
#    nb_case_x = (size_x / case_size) 
    #for x in range(nb_case_x):
        #arr.set_line( x * case_size, [255,0,0] )
    ##
    #nb_case_y = (size_y / case_size) 
    #for y in range(nb_case_y):
        #arr.set_column( y * case_size, [255,0,0] )
    ##===
    for c in g.cases.values():
        for p in c.data:
            arr.set_pixel(p.x, p.y, [0,255,255] )
    arr.save(path="screen.png")

## Build 
