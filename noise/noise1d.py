import random

from m_farges.Noise import vector2d


def gen_random_pts(min_y, max_y, max_x, min_x=0, step=10):
    result = []
    for x in range(min_x, max_x, step):
        y = random.randint(min_y, max_y)  
        v = vector2d.Vec2d(x,y)
        result.append( v )
    return (result, step)

def find_enclosing_pts( x, random_pts ):
    pts, step = random_pts
    idx = int(x / step)
    before = pts[idx]
    after  = pts[idx+1]
    return before, after

def hermite_interp(x, x1, x0):
    dist = x1 - x0
    t    = (x- x0) / dist
    v    = 3*(t*t) - 2*(t*t*t)
    return v

def get_value(x, random_pts):
    x = float(x)
    before, after = find_enclosing_pts( x, random_pts )
    percentage    = hermite_interp( x, after.x, before.x)
    y = before.y + (after.y - before.y) * percentage
    return y

def gen_pts(random_pts, max_x, min_x=0, step=1, ):
    result = []
    for x in range(min_x, max_x, step):
        y = get_value(x, random_pts)
        result.append( (x, y) )
    return result
        


