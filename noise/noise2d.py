from noise.noise1d import get_value
from render import pixel

def gen_pts(random_pts_x, random_pts_y, max_x, min_x=0, step=1, ):
    result = []
    for x in range(min_x, max_x, step):
        for y in range(min_x, max_x, step):
            vx = get_value( x, random_pts_x)
            vy = get_value( y, random_pts_y)
            z = (vx + vy) / 2
            result.append( (x, y, z) )
    return result

def make_image(pts, width, height):
    ca = pixel.ColorArray(width, height)
    max_z = max(z for (x,y,z) in pts )
    for p in pts:
        x,y,z = p
        color = [z/max_z*255] * 3
        ca.set_pixel( x, y, color ) 
