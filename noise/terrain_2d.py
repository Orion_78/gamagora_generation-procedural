# split  ::  [p1, p2], height -> [p1, pn, p2]
def split( pts, h):
    p1, p2 = pts
    # get mid-vector
    p = ( p2 - p1 ) / 2.0
    # add height along normal
    #push = p.perpendicular_normal() * h
    push = (0, h)
    ## find normal
    p = p1 + p + push
    return (p1, p , p2)

# random_height :: max_variation -> height
import random
def random_height(max_variation):
    return (random.random() - 0.5) * 2 * max_variation

# pairs  :: pts -> [ [ p1, p2 ] ]
def pairs(pts):
    last = None
    for p in pts:
        if last is None:
            last = p
        else:
            yield (last, p)
            last = p


# merge  :: [ [ p1, p2, p3 ] ] -> pts
def merge ( triplets ) :
    result = []
    for idx, triplet in enumerate(triplets):
        if idx == 0:
            result.extend(triplet)
        else:
            result.extend(triplet[1:])
    return result

# iter_once :: pts, max_height -> pts
def iter_once(pts, max_variation):
    heights = [ random_height(max_variation) for i in range(len(pts)-1) ]
    return merge ( map(lambda args : split(*args) ,  zip( pairs( pts ), heights ) ))

#iter_ntimes :: nb, pts, max_variation, decrease -> pts
def iter_ntimes(nb, pts, max_variation, decrease, ):
    for i in range(nb):
        if i!=0 :
            max_variation = max_variation * decrease
        pts = iter_once(pts, max_variation)
    return pts


